﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class timer : MonoBehaviour {

    public Slider lightSlider;
    public float clock;
    public float timerMax = 100;

    // Use this for initialization
    void Start () {
        clock = timerMax;
    }
	
	// Update is called once per frame
	void Update () {
        clock -= Time.deltaTime;
        lightSlider.value = clock;

        if (clock <= 0)
        {
            
        }
    }
}
